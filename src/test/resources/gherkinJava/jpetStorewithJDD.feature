Feature: Acceder et se connecter a l application Jpetstore
 
	Scenario Outline: Se connecter
		Given I navigate on the jpetstore Website
		When I login with login and password
		Then I access the user homepage

Examples: 
	|	login	|	password	|	user	|
	|	j2ee	|	j2ee	|	ABC	|
	|	ACID	|	ACID	|	ABC	|
		