package gherkinJava;

import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetStorewithJDDsteps {
	

	private Map<String, String> userpassword; 
	String login; 
	String password;
	String user;
		
	WebDriver driver;


@Given("I navigate on the jpetstore Website")
public void i_navigate_on_the_jpetstore_Website() throws Throwable {
	System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
	driver = new FirefoxDriver();
	driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	driver.get("https://petstore.octoperf.com/actions/Catalog.action");
}

@When("I login with login and password")
public void i_login_with_login_and_password(Map<String, String> userpassword) {
	this.userpassword=userpassword;
	
}

@Then("I access the user homepage")
public void i_access_the_user_homepage() {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}

}
