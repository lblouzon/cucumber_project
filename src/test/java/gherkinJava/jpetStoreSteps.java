package gherkinJava;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class jpetStoreSteps {
	
	WebDriver driver;

	@Given("I have a opened a browser")
	public void i_have_a_opened_a_browser() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "./rsc/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
}

	@When("I navigate to the jpetstore URL")
	public void i_navigate_to_the_jpetstore_URL() throws Throwable {
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
}

	@And("I signin with login and password")
	public void i_signin_with_login_and_password() throws Throwable {
		driver.findElement(By.xpath("//*[@id=\"MenuContent\"]/a[2]")).click();
		   driver.findElement(By.xpath("//input[@name='username']")).sendKeys("j2ee");
		   driver.findElement(By.xpath("//form/p/input[@name='password']")).clear();
		   driver.findElement(By.xpath("//form/p/input[@name='password']")).sendKeys("j2ee");
		   driver.findElement(By.xpath("//*[@id=\"Catalog\"]/form/input")).click();
	
	WebElement titre = (driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"]")));
	String welcomeTitle = titre.getText();
	System.out.println(welcomeTitle); 
	assertTrue(welcomeTitle.contains("Welcome ABC!")); 
 
}

	@Then("I access the homepage")
	public void i_access_the_homepage() throws Throwable {
	driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"]")).getText();
		System.out.println("Page title is : " + driver.getTitle());
	assertTrue(driver.getTitle().contains("JPetStore Demo"));
}
}
